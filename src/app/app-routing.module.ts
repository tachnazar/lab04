import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudentsViewComponent } from './students/view/students.view.component';
import { StudentsComponent } from './students/list/students.component';
import { StudentsAddComponent } from './students/add/students.add.component';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { FormsModule } from '@angular/forms';


const appRoutes: Routes = [
    { path: '',
      redirectTo: '/list',
      pathMatch: 'full'
    },
    {path: '**', component: FileNotFoundComponent}
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes),
        FormsModule
    ],
    exports: [
        RouterModule,
        FormsModule
    ]
})
export class AppRoutingModule {
}
